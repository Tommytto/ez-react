const path = require('path');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const isProd = 'production' === process.env.NODE_ENV;
const port = process.env.PORT || 8080;
const paths = {
    dist: path.resolve(__dirname, 'dist'),
    public: path.resolve(__dirname, 'public'),
    src: path.resolve(__dirname, 'src')
};

const htmlPlugin = new HtmlWebPackPlugin({
    filename: 'index.html',
    template: path.resolve(paths.src, 'index.html')
});


let plugins = [htmlPlugin];

if (isProd) {
    plugins = [
        new CleanWebpackPlugin([path.resolve(paths.dist, '*')]),
        new CopyWebpackPlugin([{ from: paths.public }])
    ].concat(plugins);
}

module.exports = {
    context: paths.src,
    devServer: {
        contentBase: paths.public,
        historyApiFallback: true,
        port
    },
    entry: {
        main: './index.jsx',
        vendors: ['babel-polyfill']
    },
    module: {
        rules: [
            {
                exclude: /node_modules/,
                test: /\.(js|jsx)$/,
                use: ['babel-loader']
            },
        ]
    },
    output: {
        filename: '[name].min.js',
        publicPath: '/'
    },
    plugins,
    resolve: {
        extensions: ['*', '.js', '.jsx'],
        modules: [paths.src, 'node_modules']
    }
};
