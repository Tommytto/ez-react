import React, {Component} from 'react';

class Stopwatcher extends Component {
    constructor(props) {
        super(props);

        this.timer = null;
        this.state = {
            seconds: 0,
            minutes: 0,
        };
    }

    componentDidMount() {
        this.timer = setInterval(() => {
            this.tick();
        }, 1000);
    }

    tick() {
        this.setState({
            seconds: this.state.seconds + 1,
        })
    }

    componentWillUnmount() {
        clearInterval(this.timer);
    }

    render() {
        return (
            <h1>{this.state.seconds} Seconds</h1>
        )
    }
}

export {
    Stopwatcher
};
