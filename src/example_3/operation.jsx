import React from 'react';
import PropTypes from 'prop-types';

function Operation({text, onClick}) {
    return <button
        className="big"
        type="button"
        onClick={onClick}
        style={{fontSize: "50px"}}
    >{text}</button>
}

Operation.propTypes = {
    text: PropTypes.string,
    onClick: PropTypes.func,
};

export {
    Operation,
}
