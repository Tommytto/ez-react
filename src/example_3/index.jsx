import React from 'react';
import {Operation} from 'example_3/operation';

const operationList = {
    plus: {
        text: '+',
        id: 'plus',
    },
    minus: {
        text: '-',
        id: 'minus',
    },
    mult: {
        text: '*',
        id: 'mult',
    },
};

class Calculator extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            operationId: null,
            leftNumber: 0,
            rightNumber: 0,
            result: null,
        }
    }

    handleFormSubmit = (e) => {
        e.preventDefault();
        const {leftNumber, rightNumber, operationId} = this.state;

        let result;
        switch(operationId) {
            case operationList.plus.id:
                result = leftNumber + rightNumber;
                break;
            case operationList.minus.id:
                result = leftNumber - rightNumber;
                break;
            case operationList.mult.id:
                result = leftNumber * rightNumber;
                break;
            default:
                result = leftNumber + rightNumber;
        }

        this.setState({
            result,
        })
    };

    handleChangeInput = (e) => {
        const {name, value} = e.currentTarget;
        console.log(value);
        this.setState({
            [name]: value ? parseInt(value, 10) : 0,
        })
    };

    handleChangeOperations = (operationId) => {
        this.setState({
            operationId,
        })
    };

    render() {
        const {leftNumber, rightNumber, operationId, result} = this.state;
        return (
            <form onSubmit={this.handleFormSubmit}>
                <div>
                    <label htmlFor="leftNumber">Number 1</label>
                    <input
                        type="number"
                        id="leftNumber"
                        value={leftNumber}
                        onChange={this.handleChangeInput}
                        name="leftNumber"
                        style={{fontSize: "50px"}}
                    />
                    <label htmlFor="rightNumber">Number 2</label>
                    <input
                        type="number"
                        id="rightNumber"
                        value={rightNumber}
                        onChange={this.handleChangeInput}
                        name="rightNumber"
                        style={{fontSize: "50px"}}
                    />
                </div>
                <div>
                    <Operation
                        text={operationList.plus.text}
                        onClick={() => this.handleChangeOperations(operationList.plus.id)}/>
                    <Operation
                        text={operationList.minus.text}
                        onClick={() => this.handleChangeOperations(operationList.minus.id)}/>
                    <Operation
                        text={operationList.mult.text}
                        onClick={() => this.handleChangeOperations(operationList.mult.id)}/>
                </div>
                <div>
                    <button type="submit" className="big">Посчитать</button>
                </div>
                <div>
                    <h1>{operationId}</h1>
                    <h1>Result {result}</h1>
                </div>
            </form>
        );
    }
}

export {
    Calculator,
};
