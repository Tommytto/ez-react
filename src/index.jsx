import {ButtonCOunter2} from 'example_1';
import {Stopwatcher} from 'example_2';
import React from 'react';
import ReactDOM from 'react-dom';
import {Calculator} from 'example_3';

const root = document.getElementById('root');

ReactDOM.render(
    <div style={{fontSize: '56px'}}>
        {/*<ButtonCOunter2/>*/}
        {/*<Stopwatcher/>*/}
        <Calculator/>
    </div>,
    root
);
