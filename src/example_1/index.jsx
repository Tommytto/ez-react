import React from 'react';

function ButtonCounter() {
    return <h1>Button counter</h1>;
}

class ButtonCOunter2 extends React.Component {
    constructor() {
        super();

        this.state = {
            counter: 0,
        }
    }

    onClickUp = () => {
        this.setState((prevState) => {
            return {
                counter: prevState.counter + 1,
            }
        })
    };

    onClickDown = () => {
        this.setState((prevState) => {
            return {
                counter: prevState.counter - 1,
            }
        })
    };

    render() {
        return (
            <div>
                <h1>{this.state.counter}</h1>
                <button className="big" onClick={this.onClickUp}>Добавить</button>
                <button className="big" onClick={this.onClickDown}>Убавить</button>
            </div>
        )
    }
}

export {
    ButtonCounter,
    ButtonCOunter2,
}
